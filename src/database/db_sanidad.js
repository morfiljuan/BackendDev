//==============================================================
//                          DATABASE
//==============================================================

'use strict'
const mongoose = require('mongoose');
require('dotenv').config({path:'./.env'});

const user = process.env.USER_DB;
const pass = process.env.PASS_DB;
const host = process.env.HOST_DB;
const port_db = process.env.PORT_DB;
// const name_db = process.env.NAME_DB;

// const mongo_uri = 'mongodb://'+host+':'+port_db+'/'+name_db;
// const mongo_uri = 'mongodb://'+user+pass+host+':'+port_db;
// const mongo_uri = 'mongodb://mongo:TilY4m7NWGIbZMWUt0GV@containers-us-west-53.railway.app:6253';
// const mongo_uri = 'mongodb://52.20.156.224:27017/taskapp';
// const mongo_uri = 'mongodb://admin:admin@52.20.156.224:27017';
const mongo_uri = 'mongodb://mongo:TilY4m7NWGIbZMWUt0GV@containers-us-west-53.railway.app:6253';

mongoose.connect(mongo_uri, function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log(`Successfully connected to ${mongo_uri}`);
    }
});