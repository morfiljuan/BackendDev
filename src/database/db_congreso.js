//==============================================================
//                          DATABASE
//==============================================================

'use strict'
const mongoose = require('mongoose');
const mongo_uri = 'mongodb://iswebtools.ga:40116/congreso_criminalistica_db';

mongoose.connect(mongo_uri, function(err) {
    if (err) {
        // throw err;
        console.log(err);
    } else {
        console.log(`Successfully connected to ${mongo_uri}`);
    }
});