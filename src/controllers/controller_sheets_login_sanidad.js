//==============================================================
//                      CONGRESS CONTROLLER  
//==============================================================

'use strict'
const  { GoogleSpreadsheet }  =  require ('google-spreadsheet');
const credential = require('../config/login_sanidad_credential.json');
const LoginModel = require('../models/model_login_sanidad');
require('dotenv').config({path: './.env'});

async function accessGoogleSheets() {

    const document = new GoogleSpreadsheet(process.env.ID_LOGIN_SANIDAD);
    await document.useServiceAccountAuth(credential);
    await document.loadInfo();

    const sheet = document.sheetsByIndex[0];
    const registros = await sheet.getRows();

    // Borrar
    const cleanDB = async ()=>{
        await LoginModel.deleteMany();
    }
    cleanDB();

    // Guardar
    for (let index = 0; index < registros.length; index++) {
        
        const loadDataDB = async ()=>{
            (registros[index].Loguear == null || registros[index].Loguear == 'FALSO')? registros[index].Loguear = false : registros[index].Loguear = true;
            const logPersona = new LoginModel({
                Nro: registros[index].Nro,
                Dni: registros[index].Dni,
                Jerarquia: registros[index].Jerarquia,
                Nombre: registros[index].Nombre,
                Destino: registros[index].Destino,
                CredencialPoli: registros[index].CredencialPoli,
                Loguear: registros[index].Loguear
            })
            await logPersona.save();
            }
            loadDataDB();
    }
}
accessGoogleSheets();

module.exports = {
    accessGoogleSheets: accessGoogleSheets,
}