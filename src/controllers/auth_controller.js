'use strict'
const Indice = require('../models/Indice');
const User = require('../models/Indice');
require('dotenv').config({path: './.env'});

const registerForm= (req, res) => {
    res.render('register');
}
const registerUser = async (req, res) => {
    console.log(req.body);
    try {
        res.json(req.body);
    } catch (error) {
        res.json({error: "ocurrio un error al guardar el usuario"})
    }
}
const loginForm = (req, res) => { 
    res.render('login');
 }

 const loginUser = async (req, res) => {
    const {userName, password} = req.body
    try {
        const user = await Indice.findOne({userName})
        if(!user) throw new Error ("no existe cuenta")
        if(user.ingreso) throw new Error ("ya ingreso antes")

    } catch (error) {
        
    }
 }

 module.exports = {
    loginForm,
    registerForm,
    registerUser
 };
