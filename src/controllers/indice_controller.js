//==============================================================
//                        INDICE CONTROLLER 
//==============================================================

'use strict'
const User = require('../models/model_login_sanidad');
require('dotenv').config({path: './.env'});

function authenticate (req, res) {

    const data = req.body;
    User.find({Dni: data.Dni, CredencialPoli : data.CredencialPoli},(err, user)=>{
        if(err){
            res.status(404).json({
                response: [{Loguear: true}]
            });
        } else if(user==''){
            res.status(200).json({
                response: [{Loguear: true}]
            });
         } else{
             res.status(200).json({
                 response: user
            });
         }
   })
}

function getAll(req, res) {
    User.find(function (err, user) {
    res.status(200).json({
                  response: user
            }); 
    })
}

function sendForm(req, res) {

    const data = req.body
    User.findOneAndUpdate({Dni: data.Dni, CredencialPoli : data.CredencialPoli},{Loguear: true},(err, user)=>{
        if(err){
            res.status(404).json({
                response: [{Loguear: true}]
            });
        } else if(user==''){
            res.status(200).json({
                response: [{Loguear: true}]
            });
         } else{
             res.status(200).json({
                  response: user
            }); 
         }
   })
}

module.exports = {
    authenticate,
    getAll,
    sendForm
}
