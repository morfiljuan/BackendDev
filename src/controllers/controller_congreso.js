//==============================================================
//                        CONGRESS CONTROLLER 
//==============================================================

'use strict'
const User = require('../models/model_congreso');
const  { GoogleSpreadsheet }  =  require ('google-spreadsheet');
const credential = require('../config/congress_credential.json');
const internetAvailable = require("internet-available");
require('dotenv').config({path: './.env'});

async function accessGoogleSheets(col, dni) {

    const document = new GoogleSpreadsheet(process.env.ID_CONGRESO_PRUEBA);
    await document.useServiceAccountAuth(credential);
    await document.loadInfo();
    const sheet = document.sheetsByIndex[2];

    const larryRow = await sheet.getRows();
    const row = larryRow.find(data => data.DNI == dni );
    await sheet.loadCells(col+row.rowIndex);
    let cell = sheet.getCellByA1(col+row.rowIndex);
    cell.value = true;
    await sheet.saveUpdatedCells();
    await larryRow;

}
function get_users(req,res){

    User.find({Presente:true, Mensaje:true},(err, user)=>{
        if(err){
            res.status(404).json({
                response: error
            });
        } else if(!user){ 
            res.status(200).json({
                response: false
            });
         } else{
             res.status(200).json({
                  response: user
            });
         }
    })

}

function authenticate (req, res) {

    const {DNI} = req.body;
    User.findOneAndUpdate({DNI},{Presente : true},(err, user)=>{
        if(err){
            res.status(404).json({
                response: error
            });
        } else if(!user){
            res.status(200).json({
                response: false
            });
         } else{
             res.status(200).json({
                  response: user
            });
            internetAvailable().then(function(){
                console.log("Se guardo en sheet");
                accessGoogleSheets('M',DNI);
            }).catch(function(){
                console.log("No se guardo en sheet");
            });
         }
   })

}

function update_message (req, res) {

    var arr_id = req.body;
    var user = new User();
    var resp;
    arr_id.forEach(data => {
        User.findByIdAndUpdate(data._id,{Mensaje : true},(err, user)=>{
            resp = err
        });
        internetAvailable().then(function(){
            console.log("Se guardo en sheet");
            accessGoogleSheets('N',data.DNI);
        }).catch(function(){
            console.log("No se guardo en sheet");
        });
    });
    if(resp){
        res.status(404).json({
            response: error
        });
    } else if(!user){
        res.status(200).json({
            response: false
        });
     } else{
         res.status(200).json({
            response: arr_id
        });
     }

}

function send_message (req, res) {

    User.find({Presente:true, Mensaje:false},(err, user)=>{
        if(err){
            res.status(404).json({
                response: error
            });
        } else if(!user){
            res.status(200).json({
                response: false
            });
         } else{
             res.status(200).json({
                  response: user
            });
         }
    })
    
}

module.exports = {
    update_message: update_message,
    authenticate: authenticate,
    send_message: send_message,
    get_users: get_users
}