//==============================================================
//                      SERVER CONFIGURATION
//==============================================================

const app = require('../app');
require('dotenv').config({path:'./.env'});

const puerto = process.env.PORT || 3000

const ip = process.env.IP_TRABAJO

app.listen(puerto, ()=>{
    console.log(`http://${ip}:${puerto}/index.html`);
});