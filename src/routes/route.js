//==============================================================
//                       CONGRESS ROOT  
//==============================================================

'use strict'
const router = require('express').Router();
const controller_sheets_login_sanidad = require('../controllers/controller_sheets_login_sanidad');
// const internetAvailable = require("internet-available");
const indice_controller = require('../controllers/indice_controller');
// const auth_controller = require('../controllers/auth_controller');
// const validatorExpress = require('../middlewares/validatorExpress.js');
const body = require("express-validator").body;

//router.post('/login', auth_controller.auth);
//router.post('/inicio', indice_controller.inicio);

router.post('/login', indice_controller.authenticate);
router.get('/get-all', indice_controller.getAll);
router.post('/send-form', indice_controller.sendForm);
router.get('/api/v1/actualizar', controller_sheets_login_sanidad.accessGoogleSheets);

module.exports = router;
