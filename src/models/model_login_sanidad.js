//==============================================================
//                         SANIDAD MODEL
//==============================================================

'use strict'
const mongoose  = require('mongoose');

const LoginSchema = new mongoose.Schema({
    Nro: {type: Number},
    Dni: {type: Number},
    Jerarquia: {type: String},
    Nombre: {type: String},
    Destino: {type: String},
    CredencialPoli: {type: Number},
    Loguear: {type: Boolean}
}, {versionKey: false});

const LoginModel = new mongoose.model('Login', LoginSchema);
module.exports = LoginModel;