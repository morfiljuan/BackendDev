const mongoose = require('mongoose');
const bcrypt = require('bcryptjs')
const {Schema} = mongoose;

const indiceSchema = new Schema ({
    userName: {
        type: String,
        lowercase: true,
        required: true
    },
    email: {
        type: String,
        lowercase: true,
        required: true,
        unique: true,
        index: {unique: true}
    },
    password: {
        type: String,
        required: true,
    },
    tokenConfirm: {
        type: String,
        default: null
    },
    cuentaConfirmada: {
        type: Boolean,
        default: false
    }
});
module.exports =  mongoose.model("Indice",indiceSchema);