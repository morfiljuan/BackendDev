//==============================================================
//                      SYSTEM APPLICATION
//==============================================================

'use strict'
const express = require('express');
const db = require('./database/db_sanidad');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./routes/route');
const app = express();

// SETTINGS
app.use(bodyParser.json());
app.set("json spaces", 3);
app.use(bodyParser.urlencoded({ extended: false }));

const corsOptions = {
    origin: "*",
    optionsSuccessStatus: 200,
};          
app.use(cors(corsOptions));
// SETTINGS

// form de pruebas
app.use(express.static(path.join(__dirname + '/../public'))); 

app.use("/api/v1", routes);

app.listen(80, ()=>{
    console.log(`Server all rigth`);
});

// module.exports = app; 